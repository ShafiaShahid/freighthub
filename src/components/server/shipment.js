import axios from "axios";

export function getAllShpments() {
  return new Promise((resolve, reject) => {
    axios
      .get(`http://localhost:3000/shipments/`)
      .then(response => resolve(response.data))
      .catch(error => reject(error));
  });
}
export function UpdateShpments() {
  return new Promise((resolve, reject) => {
    axios
      .get(`http://localhost:3000/shipments/`)
      .then(response => resolve(response.data))
      .catch(error => reject(error));
  });
}
export function getShipments(id) {
  return new Promise((resolve, reject) => {
    axios
      .get(`http://localhost:3000/shipments/${id}`)
      .then(response => resolve(response.data))
      .catch(error => reject(error));
  });
}

export function updateDetail(id, shipmentDetails, name) {
  debugger;
  return new Promise((resolve, reject) => {
    const modifiedDetail = {
      id: shipmentDetails.id,
      name: name,
      cargo: shipmentDetails.cargo,
      mode: shipmentDetails.mode,
      type: shipmentDetails.type,
      destination: shipmentDetails.destination,
      origin: shipmentDetails.origin,
      services: shipmentDetails.services,
      total: shipmentDetails.total,
      status: shipmentDetails.status,
      userId: shipmentDetails.userId
    };

    axios
      .put(`http://localhost:3000/shipments/${id}`, modifiedDetail)
      .then(res => resolve(res))
      .catch(error => reject(error));
  });
}
