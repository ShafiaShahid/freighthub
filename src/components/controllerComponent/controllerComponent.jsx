import React from "react";
import Shipments from "../shipment";
import ShipmentDetail from "../shipmentDetail";

class ControllerComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      component: 0,
      id: null,
      update: false
    };
  }

  onShipmentClick = id => {
    debugger;
    this.setState({
      component: 1,
      id: id
    });
  };

  onShipmentDetailClick = () => {
    debugger;
    this.setState({
      component: 0
    });
  };

  updateData = () => {
    this.setState({
      update: true,
      component: 0
    });
  };

  render() {
    const { component } = this.state;
    return (
      <React.Fragment>
        {component === 0 && (
          <Shipments onShipmentClick={this.onShipmentClick} {...this.state} />
        )}
        {component === 1 && (
          <ShipmentDetail
            {...this.state}
            {...this.props}
            updateData={this.updateData}
            onShipmentDetailClick={this.onShipmentDetailClick}
          />
        )}
      </React.Fragment>
    );
  }
}

export default ControllerComponent;
