import React from "react";

import { CardBody, Row, Col } from "reactstrap";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import { TextField, Paper, Grid, Button } from "@material-ui/core";
import LocalShipping from "@material-ui/icons/LocalShipping";
import "../styles/shipmentDetail.css";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  },
  iconRoot: {
    display: "flex",
    justifyContent: "center"
  },
  icon: {
    fontSize: 100,
    color: "#004d40"
  },
  gridRoot: {
    padding: 10
  },
  paper: {
    width: "90%",
    margin: "auto",
    marginTop: "4%",
    height: 600,
    padding: 25
  },
  buttonRoot: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "2%"
  },
  button: {
    width: 250,
    height: 60,
    backgroundColor: "#004d40",
    color: "white",
    fontSize: 18,
    marginRight: 10
  },
  cancelButton: {
    width: 250,
    height: 60,
    backgroundColor: "grey",
    color: "white",
    fontSize: 18,
    marginRight: 10
  }
}));

export default function ShipmentDetails(props) {
  const {
    shipmentDetails,
    saveDetail,
    handleChange,
    startEdit,
    editing,
    name,
    onShipmentDetailClick
  } = props;
  debugger;
  const classes = useStyles();

  //   console.log(shipmentDetails.cargo[0].type);

  return (
    <Row>
      <Col md={12} lg={12} xl={12}>
        <Paper className={classes.paper} elevation={11}>
          <CardBody>
            <div className={classes.iconRoot}>
              <LocalShipping className={classes.icon} />
            </div>
            <Grid
              container
              direction="row"
              justify="space-between"
              className={classes.gridRoot}
            >
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  fullWidth
                  label="Name"
                  value={name}
                  onChange={handleChange("name")}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                  onClick={() => {
                    if (!editing) startEdit();
                  }}
                />
              </Grid>

              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  id="standard-name"
                  label="Mode"
                  value={shipmentDetails.mode}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="ggrid-fieldrid">
                <TextField
                  disabled
                  fullWidth
                  id="standard-name"
                  label="Type"
                  value={shipmentDetails.type}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  id="standard-name"
                  label="Destination"
                  //   className={classes.textField}
                  value={shipmentDetails.destination}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  label="Origin"
                  value={shipmentDetails.origin}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  label="Total"
                  value={shipmentDetails.total}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  label="Status"
                  value={shipmentDetails.status}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
              <Grid item xs={12} md={5} className="grid-field">
                <TextField
                  disabled
                  fullWidth
                  label="User ID"
                  value={shipmentDetails.userId}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true
                  }}
                />
              </Grid>
            </Grid>
            <div className={classes.buttonRoot}>
              <Button
                className={classes.cancelButton}
                onClick={onShipmentDetailClick}
              >
                Cancel
              </Button>
              <Button className={classes.button} onClick={saveDetail}>
                Save
              </Button>
            </div>
          </CardBody>
        </Paper>
      </Col>
    </Row>
  );
}
