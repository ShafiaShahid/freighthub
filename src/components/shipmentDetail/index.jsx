import React from "react";
import { Container, Row, Col } from "reactstrap";
import { Paper } from "@material-ui/core";
import { getShipments, updateDetail, getAllShpments } from "../server/shipment";
import ShipmentDetails from "../shipmentDetail/components/shipmentDetail";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    backgroundColor: "#004d40",
    minHeight: "-webkit-fill-available",
    overflowY: "hidden"
  },
  paper: {
    backgroundColor: "#004d40"
  },
  title: {
    color: "white",
    fontSize: 26,
    margin: 30
  }
});
class ShipmentDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipmentDetails: [],
      editing: true,
      updating: false,
      name: ""
    };
  }

  componentDidMount() {
    getShipments(this.props.id)
      .then(response => {
        this.setState({
          shipmentDetails: response,
          name: response.name
        });
      })
      .catch(err => {
        alert("Error occured");
      });
  }

  startEdit = () => {
    this.setState({ editing: true });
  };

  saveDetail = () => {
    debugger;
    const { updateData } = this.props;
    const { shipmentDetails, name } = this.state;
    updateDetail(shipmentDetails.id, shipmentDetails, name)
      .then(() => {
        this.setState({
          name: this.state.name
        });
      })
      .catch(err => {
        alert("Error occured");
      });
    updateData();
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value.trimLeft() });
  };

  render() {
    const { classes } = this.props;
    return (
      <Container className={classes.root}>
        <Row>
          <Col md={12}>
            <h3 className={classes.title}> Shipment Details</h3>
          </Col>
        </Row>
        <Paper className={classes.paper}>
          <ShipmentDetails
            {...this.state}
            {...this.props}
            saveDetail={this.saveDetail}
            handleChange={this.handleChange}
            startEdit={this.startEdit}
          />
        </Paper>
      </Container>
    );
  }
}

ShipmentDetail.propTypes = {
  classes: PropTypes.node.isRequired
};

export default withStyles(styles)(ShipmentDetail);
