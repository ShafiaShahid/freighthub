import React from "react";
import ReactDOM from "react-dom";
import MUIDataTable from "mui-datatables";
import { IconButton } from "@material-ui/core";
import Edit from "@material-ui/icons/Edit";

class ShipmentTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  createRow = shipmentDetails => {
    const { onShipmentClick } = this.props;
    const newArray = [];
    shipmentDetails &&
      shipmentDetails.map(shipmentDetails => {
        const tempObjectArray = [];
        tempObjectArray.push(shipmentDetails.id);
        tempObjectArray.push(shipmentDetails.name);
        tempObjectArray.push(shipmentDetails.cargo[0].type);
        tempObjectArray.push(shipmentDetails.mode);
        tempObjectArray.push(shipmentDetails.type);
        tempObjectArray.push(shipmentDetails.destination);
        tempObjectArray.push(shipmentDetails.origin);
        tempObjectArray.push(shipmentDetails.services[0].type);
        tempObjectArray.push(shipmentDetails.total);
        tempObjectArray.push(shipmentDetails.status);
        tempObjectArray.push(
          <IconButton onClick={() => onShipmentClick(shipmentDetails.id)}>
            <Edit />
          </IconButton>
        );
        newArray.push(tempObjectArray);
      });
    return newArray;
  };

  render() {
    const columns = [
      "ID",
      "Name",
      "Cargo",
      "Mode",
      "Type",
      "Destination",
      "Origin",
      "Services",
      "Total",
      "Status",
      ""
    ];
    const { shipmentDetails } = this.props;
    const newArray = this.createRow(shipmentDetails);

    const options = {
      filterType: "dropdown",
      responsive: "scroll"
    };

    return (
      <MUIDataTable
        title={"Shipment Logs"}
        data={newArray}
        columns={columns}
        options={options}
        rowsPerPage={20}
        rowsPerPageOptions={[20]}
      />
    );
  }
}

ReactDOM.render(<ShipmentTable />, document.getElementById("root"));
export default ShipmentTable;
