import React from "react";
import Shipment from "../shipment/components/ShipmentTable";
import { Container, Row, Col } from "reactstrap";
import { Paper } from "@material-ui/core";
import "./styles/shipment.style.css";
import { getAllShpments, UpdateShpments } from "../server/shipment";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    backgroundColor: "#004d40",
    minHeight: "-webkit-fill-available",
    overflowY: "hidden"
  },
  paper: {
    backgroundColor: "#004d40",
    marginTop: "4%"
  },
  title: {
    color: "white",
    fontSize: 26,
    margin: 30
  }
});

class Shipments extends React.Component {
  constructor(props) {
    debugger;
    super(props);
    this.state = {
      shipmentDetails: [],
      startSearch: false
    };
  }

  componentDidMount() {
    debugger;
    if (this.props.update) {
      this.updateCall();
    } else {
      getAllShpments()
        .then(response => {
          this.setState({
            shipmentDetails: [...response]
          });
        })
        .catch(err => {
          alert("Error occured");
        });
    }
  }

  updateCall = () => {
    debugger;
    UpdateShpments()
      .then(response => {
        this.setState({
          shipmentDetails: [...response]
        });
      })
      .catch(err => {
        alert("Error occured");
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <Container className={classes.root}>
        <Row>
          <Col md={12}>
            <h3 className={classes.title}> FreightHub</h3>
          </Col>
        </Row>

        <Paper className={classes.paper}>
          <Shipment {...this.state} {...this.props} />
        </Paper>
      </Container>
    );
  }
}

Shipments.propTypes = {
  classes: PropTypes.node.isRequired
};

export default withStyles(styles)(Shipments);
