import React, { Component } from "react";
import "./App.css";
import ControllerComponent from "../src/components/controllerComponent/controllerComponent";

class App extends Component {
  render() {
    return <ControllerComponent />;
  }
}

export default App;
